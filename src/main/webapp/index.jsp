<%--
    Document   : index
    Created on : 2017-08-03, 19:08:21
    Author     : artisticImpresion
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <%@include file="/WEB-INF/header.jsp" %>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-9">

                    <%
                        java.util.Date date = new java.util.Date();
                    %>
                    <h7>
                        Today is: <%= date%>.
                    </h7>

                    <%@include file="/WEB-INF/menu.jsp" %>
                    <h1>Hello in CarsApp!</h1>
                    <br/><br/><br/>
                    <%@include file="display.jsp" %>

                </div>
                <div class="col-md-3">
                    <p>right side</p>
                    <br/><br/><br/><br/><br/><br/>
                    <br/><br/><br/><br/><br/><br/>
                    <br/><br/><br/><br/><br/><br/>
                    <br/><br/><br/><br/><br/><br/>
                    <br/><br/><br/><br/><br/><br/>
                    <br/><br/><br/><br/><br/><br/>
                    it's temporary ;)
                </div>
            </div>
                    


            <div class="row">
                <div class="col-md-12">
                    <%@include file="/WEB-INF/footer.jsp" %>
                </div>
            </div>
        </div><!-- container end -->
    </body>
</html>
