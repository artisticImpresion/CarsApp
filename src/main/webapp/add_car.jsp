<%-- 
    Document   : add_car
    Created on : 2017-08-03, 20:18:42
    Author     : artisticImpresion
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="/WEB-INF/header.jsp" %>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-9">

                    <%
                        java.util.Date date = new java.util.Date();
                    %>
                    <h7>
                        Today is: <%= date%>.
                    </h7>

                    <%@include file="/WEB-INF/menu.jsp" %>
        <h2>Add your car below:</h2>
        <br/>
        <form method="POST" action="<%=request.getContextPath()%>/AddCar">
            <label>Imię: <input type="text" name="first_name" /></label><br/>
            <label>Nazwisko: <input type="text" name="last_name" /></label><br/>
            <label>PESEL: <input type="text" name="pesel" /></label><br/>
            <br/>
            <p>Adres do korespondencji:</p>
            <label>Ulica z numerem: <input type="text" name="street" /></label><br/>
            <label>Kod pocztowy: <input type="text" name="zip_code" /></label><br/>
            <label>Miasto: <input type="text" name="city" /></label><br/>
            <label>Kraj: <input type="text" name="country" /></label><br/>
            <br/>
            <input type="reset" class="btn btn-primary" value="Wyczyść dane"/> lub 
            <input type="submit" class="btn btn-primary" value="Wyślij formularz"/><br/>
        </form>
        
           
           
           
           
           
                </div>
                <div class="col-md-3">
                    <p>right side</p>
                    <br/><br/><br/><br/><br/><br/>
                    <br/><br/><br/><br/><br/><br/>
                    <br/><br/><br/><br/><br/><br/>
                    <br/><br/><br/><br/><br/><br/>
                    <br/><br/><br/><br/><br/><br/>
                    <br/><br/><br/><br/><br/><br/>
                    it's temporary ;)
                </div>
            </div>



            <div class="row">
                <div class="col-md-12">
                    <%@include file="/WEB-INF/footer.jsp" %>
                </div>
            </div>
        </div><!-- container end -->
    </body>
</html>