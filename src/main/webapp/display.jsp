<%-- 
    Document   : display
    Created on : 2017-08-06, 22:00:01
    Author     : artisticImpresion
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div>
    
    <h2>
        display tests
    </h2>
    <h3>
        we are testing display...
    </h3>
    <p>
        Texts and other things...<br/>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    </p>
    
</div>
