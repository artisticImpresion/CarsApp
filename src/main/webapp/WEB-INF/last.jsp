<%-- 
    Document   : last
    Created on : 2017-08-06, 13:59:04
    Author     : artisticImpresion
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <%@include file="/WEB-INF/header.jsp" %>
    <body>
        <%@include file="/WEB-INF/menu.jsp" %>
        <h1>List of last added car owners:</h1>
        <c:forEach var='myVariable' items='${owners}'>
            <p>
                Sz.P. <c:out value='${myVariable.firstName}' /> <c:out value='${myVariable.lastName}' /><br/>
            </p>
        </c:forEach>
        <c:forEach var='myVariable2' items='${ownerAddress}'>    
            Ul. <c:out value='${myVariable2.street}' /><br/>
                <c:out value='${myVariable2.zipCode}' /> <c:out value='${myVariable2.city}' /><br/>
                <c:out value='${myVariable2.country}' /><br/>
        </c:forEach>
        
    
        <%@include file="/WEB-INF/footer.jsp" %>
    </body>
</html>
