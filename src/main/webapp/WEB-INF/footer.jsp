<%-- 
    Document   : footer
    Created on : 2017-08-06, 22:29:14
    Author     : artisticImpresion
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!-- TODO: footer below -->
<h7>All rights reserved &reg; artisticImpresion <a href="https://gitlab.com/artisticImpresion" target="_blank">https://gitlab.com/artisticImpresion</a></h7>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>