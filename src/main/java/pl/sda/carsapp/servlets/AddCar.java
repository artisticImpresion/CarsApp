/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.carsapp.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.sda.carsapp.ConfigHibernate;
import pl.sda.carsapp.model.Address;
import pl.sda.carsapp.model.Owner;

/**
 *
 * @author artisticImpresion
 */
@WebServlet
public class AddCar extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        //nothing here
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/AddCar").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        response.setContentType("text/html;charset=UTF-8");
        
        String firstName = request.getParameter("first_name");
        String lastName = request.getParameter("last_name");
        String pesel = request.getParameter("pesel");
        String street = request.getParameter("street");
        String zipCode = request.getParameter("zip_code");
        String city = request.getParameter("city");
        String country = request.getParameter("country");
        
        SessionFactory instance = ConfigHibernate.getInstance();
        Session openSession = instance.openSession();
        Transaction trans = openSession.beginTransaction();
        try {
            Owner ownerToAdd = new Owner();
            ownerToAdd.setFirstName(firstName);
            ownerToAdd.setLastName(lastName);
            ownerToAdd.setPesel(Long.parseLong(pesel));
            openSession.save(ownerToAdd);
            
            Address addressToAdd = new Address();
            addressToAdd.setStreet(street);
            addressToAdd.setZipCode(zipCode);
            addressToAdd.setCity(city);
            addressToAdd.setCountry(country);
            openSession.save(addressToAdd);
            
            
            trans.commit();
        } catch (Exception e) {
            trans.rollback();
        }
        
        //testowe przekierowanie
        request.getRequestDispatcher("/ShowLastEntry").forward(request, response);
        
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
